﻿namespace MyChevrolet
{
	public static class Constants
	{
		internal static class Routes
		{
			public const string Origin    = "https://my.chevrolet.com";
			public const string Login     = Origin + "/login";
			public const string LoginAjax = Origin + "/oc_login";

			public static string EvStats( string vin, string accountId )
				=> $"{Origin}/api/vehicleProfile/{vin}/{accountId}/evstats/false";
		}

		public static class ChargeState
		{
			public const string NotCharging      = "not_charging";
			public const string Charging         = "charging";
			public const string ChargingComplete = "charging_complete";
		}

		public static class PlugState
		{
			public const string PluggedIn = "plugged";
			public const string Unplugged = "unplugged";
		}
	}
}