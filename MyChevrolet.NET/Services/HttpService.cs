﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using MyChevrolet.Exceptions;
using Newtonsoft.Json;

namespace MyChevrolet.Services
{
	public class HttpService : IDisposable
	{
		private HttpClient http;

		public HttpService()
		{
			this.http    = new HttpClient();
			this.Cookies = new CookieContainer();
		}

		public HttpService( CookieContainer cookies )
		{
			this.http    = new HttpClient();
			this.Cookies = cookies;
		}

		public CookieContainer    Cookies        { get; }
		public HttpRequestHeaders DefaultHeaders { get; set; }

		public async Task<TResponse> GetAsync<TResponse>( string url )
		{
			var request  = this.PrepareRequest( HttpMethod.Get, url );
			var response = await this.http.SendAsync( request );

			return await this.HandleResponseAsync<TResponse>( request, response );
		}

		public async Task<TResponse> PostFormAsync<TResponse>( string url, Dictionary<string, string> formData )
		{
			var request = this.PrepareRequest( HttpMethod.Post, url );
			var form    = new FormUrlEncodedContent( formData );

			request.Content = form;

			var response = await this.http.SendAsync( request );

			return await this.HandleResponseAsync<TResponse>( request, response );
		}

		public void Dispose()
		{
			this.http?.Dispose();

			this.http = null;
		}

		private HttpRequestMessage PrepareRequest( HttpMethod method, string url )
		{
			var uri     = new Uri( url );
			var request = new HttpRequestMessage( method, uri );

			if ( this.DefaultHeaders != null )
			{
				foreach ( var header in this.DefaultHeaders )
				{
					request.Headers.Add( header.Key, header.Value );
				}
			}

			if ( request.Headers.TryGetValues( "Cookie", out _ ) )
				request.Headers.Remove( "Cookie" );

			request.Headers.Add( "Cookie", this.Cookies.GetCookieHeader( uri ) );

			return request;
		}

		private async Task<TResponse> HandleResponseAsync<TResponse>( HttpRequestMessage forRequest, HttpResponseMessage response )
		{
			try
			{
				response.EnsureSuccessStatusCode();
			}
			catch ( HttpRequestException e )
			{
				throw new HttpException( e.Message, e ) {
					Request  = forRequest,
					Response = response
				};
			}

			if ( response.Headers.TryGetValues( "Set-Cookie", out var setCookies ) )
			{
				this.Cookies.SetCookies( response.RequestMessage.RequestUri, string.Join( ",", setCookies ) );
			}

			if ( typeof( TResponse ) != typeof( string ) &&
				 response.Content.Headers.ContentType?.MediaType?.EndsWith( "/json" ) == true ||
				 response.Content.Headers.ContentType?.MediaType == "text/plain" ) // OnStar API returns JSON as "text/plain" sometimes
			{
				var responseJson = await response.Content.ReadAsStringAsync();

				return JsonConvert.DeserializeObject<TResponse>( responseJson );
			}

			if ( typeof( TResponse ) != typeof( string ) )
				throw new InvalidOperationException( $"Content type does not match requested response type of {typeof( TResponse ).FullName}" );

			var responseText = await response.Content.ReadAsStringAsync();

			return (TResponse) Convert.ChangeType( responseText, typeof( TResponse ) );
		}
	}
}