﻿using System.Linq;
using System.Threading.Tasks;
using MyChevrolet.Exceptions;
using MyChevrolet.Models;
using MyChevrolet.Models.Responses;

namespace MyChevrolet.Services
{
	public class OnStarService
	{
		public async Task<GmSession> StartSessionAsync( string loginId, string password )
		{
			using ( var http = new HttpService() )
			{
				// GET login page to set initial session cookies
				await http.GetAsync<string>( Constants.Routes.Login );

				// POST login data
				var loginResponse = await http.PostFormAsync<LoginResponse>( Constants.Routes.LoginAjax, Forms.Login( loginId, password ) );

				if ( loginResponse.ServerErrorMsgs.Any() )
				{
					var errors = string.Join( ", ", loginResponse.ServerErrorMsgs );

					throw new OnStarException( $"An error occurred while logging in: {errors}" );
				}

				if ( loginResponse.Data == null )
					throw new OnStarException( "An unknown error occurred while logging in." );

				return new GmSession {
					Cookies      = http.Cookies,
					OwnerProfile = loginResponse.Data.OwnerProfile,
					VehicleMap   = loginResponse.Data.VehicleMap
				};
			}
		}

		public async Task<EvStatsData> GetEvStatsAsync( GmSession session, Vehicle vehicle )
		{
			using ( var http = new HttpService( session.Cookies ) )
			{
				var evStatsResponse = await http.GetAsync<EvStatsResponse>( Constants.Routes.EvStats( vehicle.Vin, session.OwnerProfile.OnStarAccountId ) );

				if ( evStatsResponse.ServerErrorMsgs.Any() )
				{
					var errors = string.Join( ", ", evStatsResponse.ServerErrorMsgs );

					throw new OnStarException( $"An error occurred while retrieving stats: {errors}" );
				}

				if ( evStatsResponse.Data == null )
					throw new OnStarException( "An unknown error occurred while retrieving stats." );

				return evStatsResponse.Data;
			}
		}
	}
}