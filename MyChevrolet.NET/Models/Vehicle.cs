﻿using System;
using Newtonsoft.Json;

namespace MyChevrolet.Models
{
	[ Serializable ]
	public class Vehicle
	{
		[ JsonProperty( "vehicle_id" ) ]
		public string VehicleId { get; set; }

		public string Vin                            { get; set; }
		public string Year                           { get; set; }
		public string Make                           { get; set; }
		public string Model                          { get; set; }
		public string ModelCode                      { get; set; }
		public string OnstarAccountNumber            { get; set; }
		public bool   OnstarEnabled                  { get; set; }
		public string OnstarSubscriptionServicesName { get; set; }
		public string Odometer                       { get; set; }
		public string ImageUrl                       { get; set; }
		public string RenderImageUrl                 { get; set; }
		public string FuelType                       { get; set; }
		public string Trim                           { get; set; }
	}
}