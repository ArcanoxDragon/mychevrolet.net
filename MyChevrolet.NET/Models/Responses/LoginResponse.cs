﻿using System.Collections.Generic;

namespace MyChevrolet.Models.Responses
{
	public class LoginData
	{
		public Profile                     OwnerProfile        { get; set; }
		public Dictionary<string, Vehicle> VehicleMap          { get; set; }
		public string                      LastActiveVehicleId { get; set; }
	}

	public class LoginResponse : GmResponse<LoginData> { }
}