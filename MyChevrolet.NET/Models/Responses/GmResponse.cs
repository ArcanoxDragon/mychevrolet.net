﻿using System.Collections.Generic;

namespace MyChevrolet.Models.Responses
{
	public class GmResponse<TData>
	{
		public List<string> Message         { get; set; }
		public List<string> ServerErrorMsgs { get; set; }
		public TData        Data            { get; set; }
	}
}