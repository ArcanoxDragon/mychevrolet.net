﻿namespace MyChevrolet.Models.Responses
{
	public class EvStatsData
	{
		public ulong  DataAsOfDate           { get; set; }
		public int    BatteryLevel           { get; set; }
		public string ChargeState            { get; set; }
		public string ChargeMode             { get; set; }
		public string PlugState              { get; set; }
		public string RateType               { get; set; }
		public int    Voltage                { get; set; }
		public int    ElectricRange          { get; set; }
		public int    GasRange               { get; set; }
		public int    TotalRange             { get; set; }
		public int    FuelEconomy            { get; set; }
		public int    FuelUsed               { get; set; }
		public string EstimatedFullChargeBy  { get; set; }
		public string GasFuelLevelPercentage { get; set; }

		public string ElectricRangeUOM { get; set; }
		public string GasRangeUOM      { get; set; }
		public string TotalRangeUOM    { get; set; }
		public string FuelEconomyUOM   { get; set; }
		public string FuelUsedUOM      { get; set; }
	}

	public class EvStatsResponse : GmResponse<EvStatsData> { }
}