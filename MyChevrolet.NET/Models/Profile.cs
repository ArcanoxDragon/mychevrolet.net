﻿using System;
using Newtonsoft.Json;

namespace MyChevrolet.Models
{
	[ Serializable ]
	public class Profile
	{
		public string LoginId             { get; set; }
		public string FirstName           { get; set; }
		public string MiddleName          { get; set; }
		public string LastName            { get; set; }
		public string Address1            { get; set; }
		public string Address2            { get; set; }
		public string City                { get; set; }
		public string StateProvince       { get; set; }
		public string PostalCode          { get; set; }
		public string Country             { get; set; }
		public string Phone1              { get; set; }
		public string Phone2              { get; set; }
		public string Phone3              { get; set; }
		public string Phone4              { get; set; }
		public string Phone1Type          { get; set; }
		public string Phone2Type          { get; set; }
		public string Phone3Type          { get; set; }
		public string Phone4Type          { get; set; }
		public string ProfileId           { get; set; }
		public string LastActiveVehicleId { get; set; }

		[ JsonProperty( "onStarAcctId" ) ]
		public string OnStarAccountId { get; set; }
	}
}