﻿using System.Collections.Generic;

namespace MyChevrolet
{
	public static class Forms
	{
		public static Dictionary<string, string> Login( string username, string password )
			=> new Dictionary<string, string> {
				{ "j_username", username },
				{ "j_password", password },
				{ "ocevKey", "null" },
				{ "temporaryPasswordUsedFlag", "null" },
				{ "actc", "true" },
			};
	}
}