﻿using System;
using System.Collections.Generic;
using System.Net;
using MyChevrolet.Models;

namespace MyChevrolet
{
	[ Serializable ]
	public class GmSession
	{
		public CookieContainer             Cookies      { get; set; }
		public Profile                     OwnerProfile { get; set; }
		public Dictionary<string, Vehicle> VehicleMap   { get; set; }
	}
}