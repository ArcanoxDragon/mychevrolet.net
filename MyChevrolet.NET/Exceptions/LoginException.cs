﻿using System;

namespace MyChevrolet.Exceptions
{
	public class LoginException : OnStarException
	{
		public LoginException() { }
		public LoginException( string message ) : base( message ) { }
		public LoginException( string message, Exception innerException ) : base( message, innerException ) { }
	}
}