﻿using System;

namespace MyChevrolet.Exceptions
{
	public class OnStarException : Exception
	{
		public OnStarException() { }
		public OnStarException( string message ) : base( message ) { }
		public OnStarException( string message, Exception innerException ) : base( message, innerException ) { }
	}
}