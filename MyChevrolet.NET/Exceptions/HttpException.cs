﻿using System;
using System.Net.Http;

namespace MyChevrolet.Exceptions
{
	public class HttpException : HttpRequestException
	{
		public HttpException() { }
		public HttpException( string message ) : base( message ) { }
		public HttpException( string message, Exception inner ) : base( message, inner ) { }

		public HttpRequestMessage  Request  { get; set; }
		public HttpResponseMessage Response { get; set; }
	}
}