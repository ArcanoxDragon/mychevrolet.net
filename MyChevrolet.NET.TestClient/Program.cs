﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using MyChevrolet.Exceptions;
using MyChevrolet.Models;
using MyChevrolet.Services;

namespace MyChevrolet.NET.TestClient
{
	class Program
	{
		private static async Task Main()
		{
			var onstar   = new OnStarService();
			var username = ReadLine.Read( "OnStar Username: " );
			var password = ReadLine.ReadPassword( "Password: " );
			var session  = default( GmSession );

			try
			{
				session = await onstar.StartSessionAsync( username, password );
			}
			catch ( HttpException e )
			{
				if ( e.Response.StatusCode == HttpStatusCode.Unauthorized )
					Console.WriteLine( "Invalid username/password" );
				else
					Console.WriteLine( e );

				return;
			}

			var good          = false;
			var chosenVehicle = default( Vehicle );

			while ( !good )
			{
				Console.WriteLine( "Your vehicles:" );

				var vehicleNum = 1;
				foreach ( var vehicle in session.VehicleMap.Values )
					Console.WriteLine( $"  {vehicleNum++}. {vehicle.VehicleId}: {vehicle.Year} {vehicle.Make} {vehicle.Model} ({vehicle.Vin})" );

				Console.Write( $"Enter a vehicle number (1 - {session.VehicleMap.Count}): " );

				var input = Console.ReadLine();

				if ( int.TryParse( input, out var inputNum ) && inputNum > 0 && inputNum <= session.VehicleMap.Count )
				{
					good          = true;
					chosenVehicle = session.VehicleMap.Values.ToList()[ inputNum - 1 ];
				}
				else
				{
					Console.WriteLine( "Invalid input." );
				}
			}

			Console.Write( "Retrieving vehicle data, please wait..." );

			var evStats = await onstar.GetEvStatsAsync( session, chosenVehicle );

			Console.WriteLine( "Done.\n" );

			Console.WriteLine( $"Charge level: {evStats.BatteryLevel}% ({evStats.ElectricRange} {evStats.ElectricRangeUOM})" );
			Console.WriteLine( $"Charge status: {evStats.ChargeState} ({evStats.ChargeMode})" );
			Console.WriteLine( $"Plug status: {evStats.PlugState} @ {evStats.Voltage} V" );

			if ( evStats.ChargeState == Constants.ChargeState.Charging )
			{
				Console.WriteLine( $"Estimated full charge by: {evStats.EstimatedFullChargeBy}" );
			}
		}
	}
}